#ifndef METEOTABLEMODEL_H
#define METEOTABLEMODEL_H

#include <QAbstractTableModel>

#include <Core/UdpSpecification.h>

class MeteoTableModel : public QAbstractTableModel {
public:
  MeteoTableModel(QObject *parent = nullptr);

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role) const override;

  void setForecast(const QVector<WeatherForecastSample> &forecast);

private:
  QVector<WeatherForecastSample> _forecast;
  QList<QPixmap> pixmaps;
};

#endif // METEOTABLEMODEL_H
