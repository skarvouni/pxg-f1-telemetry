#ifndef PREFERENCESDIALOG_H
#define PREFERENCESDIALOG_H

#include <QDialog>

namespace Ui {
class PreferencesDialog;
}

class PreferencesDialog : public QDialog {
  Q_OBJECT

signals:
  void testButtons();

public:
  explicit PreferencesDialog(QWidget *parent = nullptr);
  ~PreferencesDialog() override;

private slots:
  void accept() override;

  void populateButtons();
  void test();

private:
  Ui::PreferencesDialog *ui;
};

#endif // PREFERENCESDIALOG_H
