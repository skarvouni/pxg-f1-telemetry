#ifndef SIDETOOLBAR_H
#define SIDETOOLBAR_H

#include <QBoxLayout>
#include <QGraphicsScene>
#include <QToolButton>
#include <QWidget>

class SideToolBar : public QWidget {
  Q_OBJECT

public:
  explicit SideToolBar(QWidget *parent = nullptr);

  QToolButton *addAction(const QString &name, const QSize &size = QSize(20, 20),
                         int spacing = 4);
  QToolButton *addContextMenuAction(QWidget *widget);

  void addToWidget(QWidget *widget, int xOffset = 5, int yOffset = 40);

protected:
  bool eventFilter(QObject *obj, QEvent *event) override;

private:
  QVBoxLayout *_layout;
  QGraphicsProxyWidget *_proxy;
  int _xOffset = 0;
  int _yOffset = 0;

  void updatePositionIn(QWidget *widget);
};

#endif // SIDETOOLBAR_H
