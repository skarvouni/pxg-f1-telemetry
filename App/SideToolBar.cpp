#include "SideToolBar.h"

#include <QGraphicsProxyWidget>

SideToolBar::SideToolBar(QWidget *parent) : QWidget(parent) {
  _layout = new QVBoxLayout(this);
  _layout->setContentsMargins(0, 0, 0, 0);
  _layout->setSpacing(0);
  _layout->addStretch();
}

QToolButton *SideToolBar::addAction(const QString &name, const QSize &size,
                                    int spacing) {
  auto menuButton = new QToolButton();
  menuButton->setText(name);
  menuButton->resize(size);
  menuButton->setMaximumSize(size);
  menuButton->setMinimumSize(size);

  if (spacing > 0 && _layout->count() > 1)
    _layout->insertSpacing(_layout->count() - 1, spacing);
  _layout->insertWidget(_layout->count() - 1, menuButton);

  resize(minimumSizeHint());

  return menuButton;
}

QToolButton *SideToolBar::addContextMenuAction(QWidget *widget) {
  auto menuButton = addAction(">");
  connect(menuButton, &QToolButton::clicked, this, [widget, menuButton]() {
    auto globalPos =
        menuButton->mapToGlobal(menuButton->frameGeometry().center());
    emit widget->customContextMenuRequested(widget->mapFromGlobal(globalPos));
  });

  return menuButton;
}

void SideToolBar::addToWidget(QWidget *widget, int xOffset, int yOffset) {
  widget->installEventFilter(this);
  _xOffset = xOffset;
  _yOffset = yOffset;
  updatePositionIn(widget);
}

bool SideToolBar::eventFilter(QObject *obj, QEvent *event) {
  if (event->type() == QEvent::Resize) {
    auto widget = qobject_cast<QWidget *>(obj);
    if (widget) {
      updatePositionIn(widget);
    }
  }
  return QObject::eventFilter(obj, event);
}

void SideToolBar::updatePositionIn(QWidget *widget) {
  move(widget->width() - width() - _xOffset, _yOffset);
  resize(width(), widget->height() - _yOffset);
}
