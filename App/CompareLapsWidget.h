#ifndef COMPARELAPSWIDGET_H
#define COMPARELAPSWIDGET_H

#include "CompareTelemetryWidget.h"

#include <QTreeWidget>

class CompareLapsWidget : public CompareTelemetryWidget {
  Q_OBJECT

public:
  CompareLapsWidget(QWidget *parent = nullptr);
  ~CompareLapsWidget() override = default;

protected slots:
  void browseData() override;

protected:
  void fillInfoTree(QTreeWidget *tree, const TelemetryData *data) override;
  bool showTrackTurns() const override { return true; }
};

#endif // COMPARELAPSWIDGET_H
