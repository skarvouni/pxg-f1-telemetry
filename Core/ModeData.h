#ifndef MODE_DATA_H
#define MODE_DATA_H

#include <QDataStream>
#include <QMap>

class ModeData {
public:
  ModeData();

  QMap<int, double> distancesPerMode;

  void addValue(int mode, double lapDistance);
  void finalize(double lapDistance);
  void clear();

  ModeData &operator+=(const ModeData &other);

private:
  void removeDataFromDistance(double distance);
  void compute();

  QVector<QPair<int, double>> _modeStartDistances;
};

QDataStream &operator>>(QDataStream &in, ModeData &data);
QDataStream &operator<<(QDataStream &out, const ModeData &data);

#endif // MODE_DATA_H
