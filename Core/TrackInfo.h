#ifndef TRACKINFO_H
#define TRACKINFO_H

#include "Lap.h"

#include <QList>
#include <QPair>
#include <QString>

class TrackInfo {
public:
  TrackInfo();

  bool isValid() const { return !name.isEmpty() && nbLaps > 0; }

  QString name;

  int nbLaps = 0;
  QList<QPair<int, double>> turns;
  QString layout;

  QVector<MapPoint> leftLine;
  QVector<MapPoint> rightLine;
  QVector<MapPoint> centerLine;

  bool loadFromFile(const QString &filename);
  static bool writeLineFromLap(const QString &outputFilename, const Lap *lap,
                               double offset = 0.0);

private:
  QVector<MapPoint> loadLineFromFile(const QString &filename);
};

#endif // TRACKINFO_H
