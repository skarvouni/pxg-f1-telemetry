#include "RaceResultProducer.h"

RaceResultProducer::RaceResultProducer(const QString filename) {

  file.setFileName(filename);
  if (!file.exists()) {
    if (file.open(QIODevice::Append | QIODevice::Text)) {

      QTextStream out(&file);
      out << "Pos.,Driver,Grid Position,Stops,Points,Penalty(in s)\n";
      file.close();
    }
  } else {
    qInfo() << "eimai sto else\n";
  }
}

RaceResultProducer::~RaceResultProducer() { file.close(); }

RaceResultProducer::RaceResultProducer(QDir dir, QString filename) {
  directory.setCurrent(dir.dirName());
  file.setFileName(filename);
  if (!file.exists()) {
    qInfo() << "eimai mesa sto iF\n";
    if (file.open(QIODevice::Append | QIODevice::Text)) {

      QTextStream out(&file);
      out << "Pos.,Driver,Grid Position,Stops,Points,Penalty(in s)\n";
      file.close();
    }
  }
}

void RaceResultProducer::setRaceResult(QVector<ResultOfRace> rcs) {
  if (file.open(QIODevice::Append | QIODevice::Text)) {
    QTextStream out(&file);
    for (auto r : rcs) {
      out << r.end_pos << "," << r.drivName << "," << r.start_pos << ","
          << r.pitsp << "," << r.pts << "," << r.plts << '\n';
    }
    file.close();
  }
  directory.setPath(datadirectory.dirName());
}
