#ifndef DRIVERTRACKER_H
#define DRIVERTRACKER_H

#include "ButtonsManager.h"
#include "F1Listener.h"
#include "FlashbackManager.h"
#include "Tyres.h"

#include <QDir>

class Lap;
class Stint;
class Race;
struct ResultOfRace {
  QString drivName;
  QString drivTeam;
  int end_pos;
  int plts;
  int pitsp;
  int pts;
  int start_pos;
  QVector<int> stind_laps;
  QVector<int> stind_tyres;
};

class DriverTracker : public F1PacketInterface {
public:
  DriverTracker(int driverIndex = 0, bool raceOnly = false);
  virtual ~DriverTracker() override {}

  void init(const QDir &directory);
  void setMarkButton(Button button);

  // F1PacketInterface interface
  void telemetryData(const PacketHeader &header,
                     const PacketCarTelemetryData &data) override;
  void lapData(const PacketHeader &header, const PacketLapData &data) override;
  void sessionData(const PacketHeader &header,
                   const PacketSessionData &data) override;
  void setupData(const PacketHeader &header,
                 const PacketCarSetupData &data) override;
  void statusData(const PacketHeader &header,
                  const PacketCarStatusData &data) override;
  void participant(const PacketHeader &header,
                   const PacketParticipantsData &data) override;
  void motionData(const PacketHeader &header,
                  const PacketMotionData &data) override;
  void eventData(const PacketHeader &header,
                 const PacketEventData &data) override;
  void
  finalClassificationData(const PacketHeader &header,
                          const PacketFinalClassificationData &data) override;
  ResultOfRace returnRace();

  int getDriverIndex() const { return _driverIndex; }

  void setAcquisitionMode(bool acquisitionMode);

protected:
  ResultOfRace rcs;
  bool _extendedPlayerTelemetry = false;
  int _driverIndex;
  bool _raceOnly;
  bool _isPlayer = false;
  QDir dataDirectory;
  bool driverDirDefined = false;
  QDir driverDataDirectory;
  LapData _previousLapData;
  PacketSessionData _currentSessionData;
  CarStatusData _currentStatusData;
  PacketMotionData _currentMotionData;
  bool _isLapRecorded = false;
  Lap *_currentLap = nullptr;
  Stint *_currentStint = nullptr;
  Race *_currentRace = nullptr;
  int _currentLapNum = 1;
  int _currentStintNum = 1;
  double _startErsBalance = 0;
  double _timeDiff = 0;
  double _previousTelemetryDistance = 0;
  bool _raceFinished = false;

  bool _acquisitionMode = false;

  float startPitTime = 0;

  int lastRaceSessionPassedTime = 0;

  ButtonsManager _buttons;

  TyresData<DegradationData> degradations;
  void buttonPressed(Button button);
  void buttonReleased(Button button);

  bool finishLineCrossed(const LapData &data) const;
  double averageTyreWear(const CarStatusData &carStatus) const;

  bool isRace() const;
  bool isLastRaceLap(const LapData &data) const;

  void saveCurrentStint();
  void addLapToStint(Lap *lap);

  void addLapToRace(Lap *lap, const LapData &lapData);
  void recordRaceStint(const LapData &lapData, bool isLastStint = false);
  void recordRaceLapEvents(const LapData &lapData);
  void saveCurrentRace();

  void initLap(Lap *lap, const LapData &lapData);
  void saveCurrentLap(const LapData &lapData);
  void startLap(const LapData &lapData);

  void makeDriverDir();
  void fixDriverNameForMultiplayer(Lap *lap);

  int sessionTimePassed();

  LapData leaderLapData(const PacketLapData &lapsData) const;

private slots:
  void onSessionEnd();
};

#endif // DRIVERTRACKER_H
