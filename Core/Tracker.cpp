#include "Tracker.h"
#include "Logger.h"
#include "TrackInfo.h"

#include <QDateTime>

Tracker::Tracker(QObject *parent) : QObject(parent) {
  _dataDirectory = QDir();
  _sessionDirectory = _dataDirectory;
}

void Tracker::setDataDirectory(const QString &dirPath) {
  auto newDir = QDir(dirPath);
  if (newDir != _dataDirectory) {
    QDir().mkpath(dirPath);
    _dataDirectory = QDir(dirPath);
    _lastStartedSessionUID = 0;
    qDebug() << "Data Directory: " << _dataDirectory.absolutePath();
  }
}

void Tracker::updateAutoTrackedDrivers() {
  for (auto carIndex : _autoTrackedIndexes) {
    untrackDriver(carIndex);
  }
  _autoTrackedIndexes.clear();

  if (_addPlayerTrackingOnStart)
    _autoTrackedIndexes << _header.m_playerCarIndex;

  if (_addPlayer2TrackingOnStart && _header.hasSecondaryPlayer())
    _autoTrackedIndexes << _header.m_secondaryPlayerCarIndex;

  if (_addTeammateTrackingOnStart) {
    const auto &playerData =
        _participants.m_participants[_header.m_playerCarIndex];
    int carIndex = 0;
    for (auto &driverData : _participants.m_participants) {
      if (driverData.m_teamId == playerData.m_teamId &&
          driverData.m_driverId != playerData.m_driverId) {
        _autoTrackedIndexes << carIndex;
      }

      ++carIndex;
    }
  }

  if (_addAllCarsTrackingOnStart) {
    for (int i = 0; i < 22; ++i) {
      if (!_participants.m_participants[i].m_name.isEmpty()) {
        _autoTrackedIndexes.insert(i);
      }
    }
  }

  for (auto carIndex : _autoTrackedIndexes) {
    if (!_trackedIndexes.contains(carIndex))
      trackDriver(carIndex);
  }

  if (_addAllRaceCarsTrackingOnStart && _session.isRace()) {
    for (int carIndex = 0; carIndex < 22; ++carIndex) {
      if (!_participants.m_participants[carIndex].m_name.isEmpty() &&
          !_trackedIndexes.contains(carIndex)) {
        trackDriver(carIndex, true);
        _autoTrackedIndexes.insert(carIndex);
      }
    }
  }
}

void Tracker::start() {
  if (hasSession()) {
    updateAutoTrackedDrivers();

    if (_lastStartedSessionUID != _header.m_sessionUID) {
      auto trackName = TRACK_INFO(_session.m_trackId)->name;
      auto type =
          UdpSpecification::instance()->session_type(_session.m_sessionType);

      auto dirName =
          trackName + " " + type + " - " +
          QDateTime::currentDateTime().toString("yyyy.MM.dd hh'h'mm");
      _dataDirectory.mkdir(dirName);

      _sessionDirectory = _dataDirectory;
      _sessionDirectory.cd(dirName);
    }

    for (auto &driver : qAsConst(_trackedDrivers)) {
      driver->init(_sessionDirectory);
    }

    Logger::instance()->log("TRACKING...");
    emit statusChanged("Tracking in progress...", true);
    _isRunning = true;
    _do_start = false;
    _lastStartedSessionUID = _header.m_sessionUID;
  } else {
    Logger::instance()->log("Waiting for a session ...");
    emit statusChanged("Waiting for a session...", true);
    _autoStart = true;
  }
}

void Tracker::stop() {
  Logger::instance()->log("Stopped");
  _isRunning = false;
  _autoStart = false;
  emit statusChanged("", false);
}

void Tracker::trackDriver(int index, bool raceOnly) {
  auto driver = std::make_shared<DriverTracker>(index, raceOnly);
  _trackedDrivers.append(driver);
  _trackedIndexes.insert(index);
}

void Tracker::trackPlayer() { _addPlayerTrackingOnStart = true; }

void Tracker::trackPlayer2() { _addPlayer2TrackingOnStart = true; }

void Tracker::trackTeammate() { _addTeammateTrackingOnStart = true; }

void Tracker::trackAllCars() { _addAllCarsTrackingOnStart = true; }

void Tracker::trackAllRace() { _addAllRaceCarsTrackingOnStart = true; }

void Tracker::untrackDriver(int index) {
  for (auto it = _trackedDrivers.begin(); it != _trackedDrivers.end(); ++it) {
    if ((*it)->getDriverIndex() == index) {
      _trackedDrivers.erase(it);
      break;
    }
  }

  _trackedIndexes.remove(index);
}

void Tracker::clearTrackedDrivers() {
  _trackedDrivers.clear();
  _trackedIndexes.clear();
  _addPlayerTrackingOnStart = false;
  _addPlayer2TrackingOnStart = false;
  _addTeammateTrackingOnStart = false;
  _addAllCarsTrackingOnStart = false;
  _addAllRaceCarsTrackingOnStart = false;
}

bool Tracker::hasSession() const { return _header.isValid(); }

void Tracker::setMarkButton(Button button) {
  for (const auto &tracker : qAsConst(_trackedDrivers)) {
    tracker->setMarkButton(button);
  }
}

void Tracker::telemetryData(const PacketHeader &header,
                            const PacketCarTelemetryData &data) {
  if (!_isRunning)
    return;

  for (auto &driver : qAsConst(_trackedDrivers)) {
    driver->telemetryData(header, data);
  }
}

void Tracker::lapData(const PacketHeader &header, const PacketLapData &data) {
  if (!_isRunning)
    return;

  for (auto &driver : qAsConst(_trackedDrivers)) {
    driver->lapData(header, data);
  }
}

void Tracker::sessionData(const PacketHeader &header,
                          const PacketSessionData &data) {
  _do_start = (!hasSession() && _autoStart) || _do_start;

  if (header.m_sessionUID != _header.m_sessionUID) {
    _hasParticipants = false;

    if (_isRunning) {
      Logger::instance()->log("Session changed");
      _do_start = true;
    }
  }

  _session = data;
  _header = header;

  if (_do_start && _hasParticipants)
    start();

  if (!_isRunning)
    return;

  for (auto &driver : qAsConst(_trackedDrivers)) {
    driver->sessionData(header, data);
  }
}

void Tracker::setupData(const PacketHeader &header,
                        const PacketCarSetupData &data) {
  if (!_isRunning)
    return;

  for (auto &driver : qAsConst(_trackedDrivers)) {
    driver->setupData(header, data);
  }
}

void Tracker::statusData(const PacketHeader &header,
                         const PacketCarStatusData &data) {
  if (!_isRunning)
    return;

  for (auto &driver : qAsConst(_trackedDrivers)) {
    driver->statusData(header, data);
  }
}

void Tracker::participant(const PacketHeader &header,
                          const PacketParticipantsData &data) {
  _participants = data;
  _hasParticipants = !data.m_participants.isEmpty();

  if (_do_start && _hasParticipants)
    start();

  if (!_isRunning)
    return;

  for (auto &driver : qAsConst(_trackedDrivers)) {
    driver->participant(header, data);
  }
}

void Tracker::motionData(const PacketHeader &header,
                         const PacketMotionData &data) {
  if (!_isRunning)
    return;

  for (auto &driver : qAsConst(_trackedDrivers)) {
    driver->motionData(header, data);
  }
}

void Tracker::eventData(const PacketHeader &header,
                        const PacketEventData &data) {
  if (!_isRunning)
    return;

  for (auto &driver : qAsConst(_trackedDrivers)) {
    driver->eventData(header, data);
  }
}

void Tracker::finalClassificationData(
    const PacketHeader &header, const PacketFinalClassificationData &data) {
  Q_UNUSED(header)
  Q_UNUSED(data)

  for (auto &driver : qAsConst(_trackedDrivers)) {
    driver->finalClassificationData(header, data);
    rcs.push_back(driver->returnRace());
  }
  qInfo() << rcs.size() << "Number of drivers";
  int temp = 1;
  int i = 0;
  bool flag = true;
  while (temp < rcs.size()) {
    while (i < rcs.size() || flag == true) {
      if (rcs[i].end_pos == temp) {
        std::swap(rcs[i], rcs[temp - 1]);
        temp++;
        flag = false;
      }
      i++;
    }
    flag = true;
    i = 0;
  }
  auto fileName = "Results.csv";
  auto filePath = _sessionDirectory.absoluteFilePath(fileName);
  auto _rrp = new RaceResultProducer(filePath);
  _rrp->setRaceResult(rcs);
  rcs.erase(rcs.begin(), rcs.end());
}
