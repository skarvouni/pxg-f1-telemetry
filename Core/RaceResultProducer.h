#ifndef RACERESULTPRODUCER_H
#define RACERESULTPRODUCER_H
#include "DriverTracker.h"
#include "Race.h"
#include <QDebug>
#include <QFile>
#include <QString>
#include <QTextStream>

struct ResultOfRace;

class RaceResultProducer {

public:
  RaceResultProducer(const QString filename);
  ~RaceResultProducer();
  RaceResultProducer(QDir dir, const QString something);
  void createFile();
  void createFile(const QString filename);
  void setRaceResult(QVector<ResultOfRace> rcs);
  void setDirectory(QDir dir);
  void produceFile();

private:
  QFile file;
  QTextStream out;
  QDir directory, datadirectory;
  QString filename;
};

#endif // RACERESULTPRODUCER_H
